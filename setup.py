from setuptools import setup

install_requires = [
    'pysnooper==0.1.0',
]

setup( 
    name='nmap-wrapper',
    version='2.8.3',
    description='Wrapper of NMAP native Linux command...Listens remote requests for local network',
    long_description='Not given yet !',
    author='Miltos Vimplis',
    #url='https://gitlab.com:mvimplis2013/local-data-collector',
    url='https://gitlab.com:robert.berger/nmap-agent',
    license='MIT License',
    packages=['nmapwrapper'],
    #package_data=[],
    entry_points={
        'console_scripts': [
            'nmap_wrapper3=nmapwrapper.dummyCC.test:main',
        ]
    }, 
    #install_requires=install_requires,
    #extras_require={},
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: Console',
        'Programming Language :: Python :: 3.4',
        'Topic :: System :: Network :: Monitoring',
    ],
)
